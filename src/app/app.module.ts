import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';
import { InjectionToken } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { OpenWeatherService } from './services/open-weather.service';
import {HttpModule} from '@angular/http';
import { CitiesWeatherListComponent } from './components/cities-weather-list/cities-weather-list.component';

@NgModule({
  declarations: [
    AppComponent,
    CityWeatherComponent,
    CitiesWeatherListComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    ChartsModule
  ],
  providers: [OpenWeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
