import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {DOCUMENT} from '@angular/common';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class OpenWeatherService {

  private baseUrl = "http://api.openweathermap.org/data/2.5/weather?";
  private apiKey = "dc4a9b12c07f63c0afd4f507c02bcf8d";
  private window = null;
  private interval = {};
  observableCitiesValues = {};
  obserbableCities = {};
  url = "";
 
  constructor(private http:Http, @Inject(DOCUMENT) private document) { 

    // getting window object to access localstorage and clear intervals
    if(document.parentWindow) {
      this.window = document.parentWindow;
    } else if(document.defaultView) {
      this.window = document.defaultView;      
    } else {
      alert('No hay soporte para localStorage')
    }
  }

  /*
    Set an interval for refresh the city information gettef from
    openweathermap

    @param String city, City name to refresh.
    @param Number city, time in millisecons to get information from the api.
  */
  setRefreshTask(city:string, refreshMilliseconds:number) {
    let httpObject = this.http;
    this.window.httpObject = this.http;
    let baseUrl = this.baseUrl;
    let apiKey = this.apiKey;


    if(this.interval[city]) {
      this.window.clearInterval(this.interval[city]);
    }    
    this.interval[city] = (setInterval( function()  {
      let url = baseUrl + `q=${city}&units=metric&lang=es&appid=${apiKey}`;
      let urlEncoded = encodeURI(url);
      this.window.observableCitiesValues = this.observableCitiesValues;
      this.httpObject.get( urlEncoded )
        .map( res => {
          let response = res.json();
          let dataFromStorage = (this.window.localStorage.temps === undefined) ? 
                                                        {} : JSON.parse(this.window.localStorage.temps);

          this.observableCitiesValues[city].next(response);

          if(dataFromStorage[response.name]) {
            dataFromStorage[response.name].push(response.main.temp);
          } else {
            dataFromStorage[response.name] = [response.main.temp]
          }

          this.window.localStorage.temps = JSON.stringify(dataFromStorage);

        }).subscribe()
      }, refreshMilliseconds));
  }


  /*
    This method forces a request to the openweathermap
    @param String city, City name to get information.
  */
  getWeatherInformationByCity(city:string) {
    
    // convert the data into observable object to suscribe it from the city-weather component
    this.observableCitiesValues[city] = new Subject<string>();
    this.obserbableCities[city] = this.observableCitiesValues[city].asObservable();

    // put the observable data in window scope to share it to other functions.
    this.window.observableCitiesValues = this.observableCitiesValues;
    
    let url = this.baseUrl + `q=${city}&units=metric&lang=es&appid=${this.apiKey}`;

    return this.http.get( url )
            .map( res => {
              return res.json();
            })
  }

  /*
    This method retrieve the information for the City stored in the localstoragee
    @param String city, City name to get information.
  */
  getWeatherInformationFromStorage(city:string) {
    let dataFromStorage = (this.window.localStorage.temps === undefined) ? 
    {} : JSON.parse(this.window.localStorage.temps)[city];

    return dataFromStorage;

  }
}
