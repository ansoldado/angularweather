import { Component, Input, OnInit } from '@angular/core';
import { OpenWeatherService } from '../../services/open-weather.service';
import { trigger,state,style,animate,transition } from '@angular/animations';

@Component({
  selector: 'city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.css'],
  animations: [
    trigger('cityState', [
      state('inactive', style({
      })),
      state('active',   style({
        transform: 'translateX(-100%)',
      })),
      transition('inactive => active', animate('1000ms ease-in')),
      transition('active => inactive', animate('1000ms ease-out'))
    ])
  ]
})

export class CityWeatherComponent implements OnInit{
  @Input() city: any;
  cityImage = "";
  information : any = {};
  weatherImageBase = "http://openweathermap.org/img/w/";
  weatherImage = "";
  weaterService:OpenWeatherService = null;
  storeData = [];
  refreshTime = 180000;
  symbol = "+"

  public lineChartData:Array<any> = [
    {data: 0, label: 'Temperatura'}
  ];
  public lineChartLabels:Array<any> = [null];
  public lineChartOptions:any = {
    responsive: false
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'orange',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = false;
  public lineChartType:string = 'line';

  constructor(weatherService:OpenWeatherService) { 
    this.weaterService = weatherService;
    this.weatherImage = this.weatherImageBase + "10d.png";
  }

  /*
    Init the component information:
      - Chart atributes. 
      - City information from openweathermap.org trougth open-weather.service
  */
  ngOnInit () {
    this.cityImage = 'url("../assets/' + this.city.name.replace(/ /g, '') + '.png")';
    this.weaterService.setRefreshTask(this.city.name, this.refreshTime);
    this.weaterService.getWeatherInformationByCity(this.city.name)
                                        .subscribe(( information:any ) => {
                                          this.information = information;
                                          this.weatherImage = this.weatherImageBase + information.weather[0].icon + ".png";                                    
                                          this.storeData = this.getDiferentValues(dataFromStore, 10);                                          
                                          this.setChartData();                                          
                                        });
    this.weaterService.obserbableCities[this.city.name].subscribe( 
      (refreshInformation) => {
        this.information = refreshInformation;
        let dataFromStore = this.weaterService.getWeatherInformationFromStorage(this.information.name);
        
        if(dataFromStore != undefined)
          this.storeData = this.getDiferentValues(dataFromStore, 10);
        
        this.setChartData();
      }
    )

    let dataFromStore = this.weaterService.getWeatherInformationFromStorage(this.city.name);
    
    if(dataFromStore != undefined)
      this.storeData = this.getDiferentValues(dataFromStore, 10);
    
    this.lineChartData = [
      {data: this.storeData, label: 'Temperatura'}
    ];

    this.lineChartLabels = this.storeData.map(() =>{ return ''});
  }

  /*
    Method to update the chart data to the this.storeData values.
  */
  setChartData() {
    if(this.storeData && this.storeData.length > 0) {
      // Update the chart data for the new values.
      this.lineChartData[0].data = this.storeData;
      this.lineChartLabels = this.storeData.map(() =>{ return ''});
    } else {
      console.log('Default values');      
      this.lineChartData = [{data: 0, label: 'Temperatura'}];  
      this.lineChartLabels = [null];
    }
  }

  /*
    Method for onchage in the slider control.
  */
  changeRefresh(time) {
    this.refreshTime = time;
    this.weaterService.setRefreshTask(this.city.name, this.refreshTime);    
  }

  /*
    This method get the diferent values stored in the localstorage for this city.

    @param Array array. All data stored in the localstorage for this city
    @param Number valuesToGet. Number of diferent values to be getted from the array.
  */
  getDiferentValues(array, valuesToGet) {
    let resultArray = [];
    let contained = false;

    if(array) {
      for(let i = (array.length - 1); i >= 0 && resultArray.length < valuesToGet; i--) {
        contained = false;      
        
        // gets if the value is stored in the result array.
        for(let x = 0; x < resultArray.length; x++) {
          if(resultArray[x] == array[i]) {
            contained = false;
          }
        } 
        
        if(!contained) {
          resultArray.push(array[i]);
        }
      }
    } 

    return resultArray;
  }

  /*
    Method to change the component state to be animated.
  */
  showDetails() {
    if (this.city.state == 'active'){
      this.city.state = 'inactive';
      this.symbol = '+';
    } else {
      this.city.state = 'active';
      this.symbol = '-';      
    } 
  }
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
}
