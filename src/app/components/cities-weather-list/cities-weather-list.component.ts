import { Component } from '@angular/core';
import { NgStyle } from '@angular/common';

@Component({
  selector: 'cities-weather-list',
  templateUrl: './cities-weather-list.component.html',
  styleUrls: ['./cities-weather-list.component.css']
})
export class CitiesWeatherListComponent {
  cities = [
    {name: 'Buenos Aires', state: 'inactive'},
    {name: 'Santiago', state: 'inactive'},
    {name: 'Lima', state: 'inactive'}, 
    {name: 'Sao Paolo', state: 'inactive'}
  ];
}
