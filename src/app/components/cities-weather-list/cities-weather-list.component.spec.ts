import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitiesWeatherList.ComponentComponent } from './cities-weather-list.component.component';

describe('CitiesWeatherList.ComponentComponent', () => {
  let component: CitiesWeatherList.ComponentComponent;
  let fixture: ComponentFixture<CitiesWeatherList.ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitiesWeatherList.ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitiesWeatherList.ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
